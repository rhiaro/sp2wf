# Springpad to Workflowy

## Use

Export your Springpad data as a json file, and run this script on it to output an OPML file. Open the latter in a text editor, copy, and paste it into Workflowy, which will process it into bullet points for you.

It won't work if it generates more than 250 points, which is the free Workflowy limit. You'd have to upgrade to pro for more, or seek out another management tool that takes OPML (there are a few; more on this later).

## TODO

* Fix bug with notes that should be in multiple notebooks.
* Have it take commandline input for filenames.
* Tidy.