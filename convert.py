import json
from bs4 import BeautifulSoup

def tags_to_string(tags):
	if tags:
		tags = [t.replace(" ", "-") for t in tags]
		return "#" + ", #".join(tags)
	else:
		return ""

doc = BeautifulSoup("""
	<?xml version=\"1.0\"?><opml version=\"2.0\">
		<head>
    		<ownerEmail>amy@rhiaro.co.uk</ownerEmail>
  		</head>
  		<body>
  		</body>
  	</opml>""", "xml")

body = doc.body

json_data = open('export.json')

data = json.load(json_data)

notebooks = []
notes = []

# Separate notebooks from notes/bookmarks
for thing in data:
	if thing["type"] == "Notebook":
		notebooks.append(thing)
	else:
		notes.append(thing)

# Add notebooks as top level outlines
# uuid temporarily stored in id attribute so we can add their children later.
# Tags in a note; all other properties discarded.
for notebook in notebooks:
	new_outline = doc.new_tag("outline", 
								text=notebook["name"], 
								id=notebook["uuid"],
								_note=tags_to_string(notebook["tags"]))
	body.append(new_outline)

for note in notes:
	# Clean up note text, including replacing <br>s with &#10; that WF uses.
	fix_text = ""
	if "text" in note:
		fix_text = note["text"].replace("<br>", "&#10;")
	if "url" in note:
		fix_text = fix_text + "&#10;" + note["url"] + " #bookmark"
	all_text = fix_text + "&#10;&#10;" + tags_to_string(note["tags"])
	# Save reference to attachments as reminder to upload them somewhere and replace with a link.
	if "attachments" in note:
		atts = []
		for att in note["attachments"]:
			atts.append(att["name"])
		attstr = ", ".join(atts)
		all_text = all_text  + "&#10;&#10; #todo #attachment " + attstr
	# Make outlines
	new_outline = doc.new_tag("outline",
								text=note["name"],
								_complete=note["complete"],
								_note=all_text
								)
	# Do checklists properly
	if note["type"] == "CheckList":
		for item in note["items"]:
			item_outline = doc.new_tag("outline",
										text=item["name"],
										_complete=item["complete"]
										)
			new_outline.append(item_outline)

	# Append outline to correct parent outline(s) according to notebook uuid
	if note["notebooks"]:
		for uuid in note["notebooks"]:
			notebook_outline = body.find("outline", {"id": uuid})
			notebook_outline.append(new_outline)
			# TODO: BS append moves element rather than cloning; fix!

out = doc.prettify("utf-8")
with open("out.opml", "wb") as file:
    file.write(out)